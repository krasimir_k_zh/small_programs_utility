##FATCAL Frenqence At Trace Calculator
#!/usr/bin/python
import tkinter
from tkinter import*
from tkinter import ttk
# function for calculating frenqecy
def calculate(*args):
    try:
    # intercepting the variables from the GUI and changing it if nesesary
        gradiqulecountint = float(gradiqulecount.get())
        rangeint = float(setrange.get())
        magnificationint = float(magnification.get())
        magnetudeint = magnetude.get()
        resultMagnetudeint = resultMagnetude.get()

        # deturmin the magnetude and creating new variabl
        
        if 'ms' in magnetudeint:
            magnetudefl = 0.001
        if 'us' in magnetudeint:
            magnetudefl = 0.000001
        if 'ns' in magnetudeint:
            magnetudefl = 0.000000001

        # calculation
        
        resultint =1/((gradiqulecountint*(rangeint / magnificationint))*magnetudefl)
        # magnetude of the result
        if 'Hz' in resultMagnetudeint:
            resultMagnetudefl = 1
        if 'KHz' in resultMagnetudeint:
            resultMagnetudefl = 1000
        if 'MHz' in resultMagnetudeint:
            resultMagnetudefl = 1000000
        resultint = resultint/resultMagnetudefl
		# sending the result of calculation to Gui
        result.set(resultint)
    except ValueError:
        pass
# Main windos
root = Tk()
root.resizable(width=False, height=False)
root.title("FATCALCK")
mainframe = ttk.Frame(root, padding="3 3 3 3") #3 3 3 12
mainframe.grid(column=0, row=0, sticky=(N, W, E, S))
mainframe.columnconfigure(0, weight=1, pad=10)
mainframe.rowconfigure(0, weight = 1, pad = 10)

# GUI variables
gradiqulecount = StringVar() 
setrange= StringVar()        
magnification = StringVar()  
magnetude= StringVar()       
result = StringVar() 
resultMagnetude = StringVar()

ttk.Label(mainframe, text = "gradiqulecount  ").grid(column=0, row=0, sticky=(N,E))
gradiqulecount_entry = ttk.Entry(mainframe, width =7, textvariable = gradiqulecount)
gradiqulecount_entry.grid(column = 1, row = 0, sticky = (N, W))
gradiqulecount_entry.get()

ttk.Label(mainframe, text = "range ").grid(column=2, row= 0, sticky=(N, E))
rainge_entry = ttk.Entry(mainframe, width=7, textvariable = setrange)
rainge_entry.grid(column = 3, row = 0, sticky = (N, E))

ttk.Label(mainframe, text = "magnetude  ").grid(column=4, row=0, sticky=(N,E)) 
magnetude_entry = ttk.Combobox(mainframe,width = 3, textvariable= magnetude) 
magnetude_entry['values'] = ('ns', 'us', 'ms')                              
magnetude_entry.state(['readonly'])                                          
magnetude_entry.grid(column = 5, row = 0, sticky = (N, E))
magnetude_entry.set('ns')
ttk.Label(mainframe, text = "FRENQUENCY = ").grid(column=0, row=1, sticky=(N,W))
output_result = ttk.Label(mainframe, text = "frenqence",textvariable = result).grid(column=1, row=1, sticky=(N, W))


ttk.Label(mainframe,text = 'result in ').grid(column = 2, row =1 ,sticky=(N,W))
resultMagnetude_enty = ttk.Combobox(mainframe,width = 3, textvariable= resultMagnetude)
resultMagnetude_enty['values']= ('Hz', 'KHz', 'MHz')
resultMagnetude_enty.state(['readonly'])
resultMagnetude_enty.grid(column = 3 ,row = 1, sticky =(N, W) )
resultMagnetude_enty.set('Hz')

ttk.Label(mainframe,text = 'magnification  X ').grid(column = 4, row =1 ,sticky=(N,W))
magnification_entry = ttk.Combobox(mainframe, width =3, textvariable = magnification)
magnification_entry['values']= ('1', '5', '10')
magnification_entry.state(['readonly'])
magnification_entry.grid(column=5 ,row=1, sticky = (N,E))
magnification_entry.current(0)

ttk.Button(mainframe,text = 'CALCULATE', command = calculate).grid(column = 0, row = 2, columnspan = 6 ,sticky  =(N, E, S, W) )

root.mainloop()
